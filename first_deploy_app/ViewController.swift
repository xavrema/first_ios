//
//  ViewController.swift
//  first_deploy_app
//
//  Created by Xavier Remacle on 9/14/18.
//  Copyright © 2018 Xavier Remacle. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Hello");
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var labelToBeChanged: UILabel!
    
    @IBOutlet weak var inputText: UITextField!
    
    @IBAction func handleSubmit(_ sender: UIButton) {
        
        print("Button Clicked")
        labelToBeChanged.text = inputText.text
    }

}

